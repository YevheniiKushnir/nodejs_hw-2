const express = require("express");
const Router = express.Router();

const { getUser, changeUserPassword, deleteUserById } = require("../services/user");

Router.get("/me", getUser);
Router.patch("/me", changeUserPassword);
Router.delete("/me", deleteUserById);

module.exports = Router;
