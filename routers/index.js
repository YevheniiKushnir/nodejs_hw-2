const express = require("express");
const Router = express.Router();

const secureRoutes = require("../middlewares/secureRoutes");

const auth = require("./auth");
const notes = require("./notes");
const user = require("./user");
//

Router.use("/auth", auth);
Router.use("/notes", secureRoutes, notes);
Router.use("/users", secureRoutes, user);

module.exports = Router;
