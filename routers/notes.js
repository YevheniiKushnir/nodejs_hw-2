const express = require("express");
const Router = express.Router();

const {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  completedNoteById,
  deleteNoteById,
} = require("../services/notes");

Router.get("/", getNotes);
Router.get("/:id", getNoteById);

Router.post("/", addNote);

Router.put("/:id", updateNoteById);
Router.patch("/:id", completedNoteById);

Router.delete("/:id", deleteNoteById);

module.exports = Router;
