require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const Path = require("path");

const app = express();
const mongoose = require("mongoose");

const rootRouter = require("./routers");

app.use(express.json());
app.use(morgan("tiny"));
app.use(express.static(Path.join(__dirname, "client")));

app.use("/api", rootRouter);
app.get("/*", (req, res, nex) => {
  res.sendFile(Path.join(__dirname, "client", "index.html"));
});

const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_CONNECT);

    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error("err: ", err);
  res.status(500).send({ message: "Server error" });
}
