const User = require("../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const getDate = require("../helpers/getDate");

const register = async (req, res, next) => {
  try {
    const { username, password } = req.body;

    if (username.length === 0 || password.length === 0) throw Error("Bad credentials");

    const user = new User({
      createdDate: getDate(),
      username,
      password: await bcrypt.hash(password, 10),
    });

    user
      .save()
      .then((saved) =>
        res.json({
          message: "Success",
        })
      )
      .catch((err) => {
        res.status(400).json({ message: "User exist" });
      });
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

const login = async (req, res, next) => {
  try {
    const { username, password } = req.body;
    if (username.length === 0 || password.length === 0) throw Error("Bad credentials");

    const user = await User.findOne({ username });
    if (!user) throw Error("User not exist");

    const comparePasswords = await bcrypt.compare(String(password), String(user.password));

    if (user && comparePasswords) {
      const payload = { username: user.username, userId: user._id };

      const jwtToken = jwt.sign(payload, process.env.JWT_SECRET_KEY);

      return res.json({
        message: "Success",
        jwt_token: jwtToken,
      });
    } else throw Error("Not authorized");
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

module.exports = {
  register,
  login,
};
