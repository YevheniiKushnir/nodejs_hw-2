const Note = require("../models/Note");
const getDate = require("../helpers/getDate");

const getNotes = async (req, res, nex) => {
  try {
    const { offset = 0, limit = 0 } = req.query;

    const { userId } = req.user;
    const notes = await Note.find({ userId }, "-__v");

    let newNotes = null;
    if (offset !== 0 && limit !== 0) {
      newNotes = notes.slice(Number(offset), Number(offset) + Number(limit));
    }

    res.json({
      offset: Number(offset),
      limit: Number(limit),
      count: notes.length,
      notes: newNotes ?? notes,
    });
  } catch (error) {
    res.status(400).json({ message: "Bad request" });
  }
};

const addNote = async (req, res, nex) => {
  try {
    const { text } = req.body;
    const { userId } = req.user;

    if (text.length === 0) throw Error("Empty text");

    const note = new Note({
      userId,
      createdDate: getDate(),
      text,
    });

    await note.save();

    res.json({
      message: "Success",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const getNoteById = async (req, res, nex) => {
  try {
    const { id } = req.params;
    const { userId } = req.user;
    const note = await Note.findById({ _id: id }, "-__v");

    if (note.userId !== userId) throw Error("Access error");

    res.json({
      note,
    });
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

const updateNoteById = async (req, res, nex) => {
  try {
    const { id } = req.params;
    const { userId } = req.user;
    const { text } = req.body;

    const note = await Note.findById({ _id: id }, "-__v");

    if (note.userId !== userId) throw Error("Access error");

    if (text) note.text = text;

    await note.save();

    res.json({
      message: "Success",
    });
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

const completedNoteById = async (req, res, nex) => {
  try {
    const { id } = req.params;
    const { userId } = req.user;

    const note = await Note.findById({ _id: id }, "-__v");

    if (note.userId !== userId) throw Error("Access error");

    note.completed = !note.completed;

    await note.save();

    res.json({
      message: "Success",
    });
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

const deleteNoteById = async (req, res, nex) => {
  try {
    const { id } = req.params;
    const { userId } = req.user;

    const note = await Note.findById({ _id: id }, "-__v");

    if (note.userId !== userId) throw Error("Access error");

    await note.delete();

    res.json({
      message: "Success",
    });
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

module.exports = {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  completedNoteById,
  deleteNoteById,
};
