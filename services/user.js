const bcrypt = require("bcryptjs");

const User = require("../models/User");

const getUser = async (req, res, next) => {
  try {
    const { userId } = req.user;

    const { _id, username, createdDate } = await User.findById(userId);

    res.json({
      user: {
        _id,
        username,
        createdDate,
      },
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const changeUserPassword = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const { oldPassword, newPassword } = req.body;

    const user = await User.findById(userId);

    const verifyUser = bcrypt.compare(String(oldPassword), String(user.password));

    if (newPassword && verifyUser) {
      const hashedPassword = await bcrypt.hash(newPassword, 10);
      user.password = hashedPassword;

      await user.save();
    } else throw Error("Access error");

    res.json({
      message: "Success",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const deleteUserById = async (req, res, next) => {
  try {
    const { userId } = req.user;

    const user = await User.findById(userId);

    await user.delete();

    res.json({
      message: "Success",
    });
  } catch (error) {
    res.status({ message: error.message || "Bad request" });
  }
};

module.exports = {
  getUser,
  changeUserPassword,
  deleteUserById,
};
