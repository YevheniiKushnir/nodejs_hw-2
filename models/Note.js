const mongoose = require("mongoose");

const note = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
});

const Note = mongoose.model("Note", note);

module.exports = Note;
